Limitations {#limitations}
===========
The Point class does not have input functionality compatible with the iostream library. The Vector2D class does not have
functionality for operators such as *=, += and -=. The input for a vector using the iostream library does not check that the
input given by the user is a valid data type. The Circle class does not yet have functionality that allows for the center or
radius of the circle to be changed.
