Problem statement
=================

The programmer shall create a Point class, a Vector2D class and a Circle class. All classes shall be able to be used in an
external file by using the #inclue command.

The Point class shall represent a point in \f$ \mathbb{R}^2 \f$. Hence, it will contain the \f$ (x,y)\f$ position of the point.

The Vector2D class shall represent a vector in \f$ \mathbb{R}^2 \f$. Hence, it shall contain the \f$ [x,y]\f$ components of the vector.
The Vector2D class shall have functionality that will allow it to:
- Determine the norm of the vector
- Display the vector in a form compliant with the iostream library
- Set the values of the components of the vector using the iostream library
- Use the subscript operator to access the components
- Return the maximum component of the vector
- Return the minimum component of the vector
- Apply vector addition
- Apply vector subtraction
- Calculate the dot product of two vectors
- Test for equality and inequality of two vectors
- Scalar multiplication of a vector

The Circle class shall represent a circle in \f$ \mathbb{R}^2 \f$. This shall be done by containing the center of the
circle as a point and the radius of the circle in the class. The circle shall be able to be constructed by inputting the
center of the circle and the radius of the circle and it shall also be able to be constructed by inputting three points
that lie on the circumference of the circle.
