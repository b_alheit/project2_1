Verification
===========
An example of a main file that displays all the functionality required as per the problem statement is shown in the figure
below. Note that although the Point class is not explicitly used in this example it is used in the Circle class. Hence, some
of its functionality is displayed when the method get_centre() is called as the center of a Circle object is a point.

![the caption](..\doc\example_main.png)

An example output from this file is shown in the figure below. The inputs were chosen to be simple numbers so that one
can easily verify to themselves that the answers produced by the code are in fact correct.

![the caption](..\doc\example_out.png)