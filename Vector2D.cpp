#include "Vector2D.h"
#include <cmath>
using namespace std;

Vector2D::Vector2D() {
    x = 0; // Set x component to 0
    y = 0; // Set y component to 0
}

Vector2D::Vector2D(double xInit, double yInit) {
    x = xInit; // Set x component to chosen initial x value
    y = yInit; // Set y component to chosen initial y value
}

Vector2D::Vector2D(Point p) {
    x = p.get_x(); // Set x component to value of x position of input point
    y = p.get_y(); // Set y component to value of y position of input point
}

double Vector2D::max() {
    if(x > y) // If the x component is larger than the y component then return the x component, otherwise return the y component
        return x;
    else
        return y;
}

double Vector2D::min() {
    if (x<y) // If the x component is less than the y component then return the x component, otherwise return the y component
        return x;
    else
        return y;
}

double Vector2D::norm() {
    return pow(x*x + y*y, 0.5); // Applying the equation to get the norm of a 2D vector
}


ostream& operator<<(ostream &out, const Vector2D& vec) {
    out << "[" << vec.x << "," << vec.y << "]"; // Constructing an ostream reference displaying the vector using square brackets
    return out;
}

istream& operator>>(istream& in, Vector2D& vec) {
    in >> vec.x >> vec.y; // Inputting the values for the x and y components of the vector respectively
    return in;
}

Vector2D operator+(const Vector2D &lhs, const Vector2D &rhs) {
    Vector2D vec(lhs.x + rhs.x, lhs.y + rhs.y); // Creating a new vector with components that are the addition of the corresponding components of the input vectors
    return vec;
}

Vector2D operator-(const Vector2D &lhs, const Vector2D &rhs) {
    Vector2D vec(lhs.x - rhs.x, lhs.y - rhs.y); // Creating a new vector with components that are the subtraction of the corresponding components of the left input vector from the right input vector
    return vec;
}

double operator*(const Vector2D &lhs, const Vector2D &rhs) {
    return lhs.x * rhs.x + lhs.y * rhs.y; // Applying the equation for the dot product of two 2D vectors
}

Vector2D operator*(const Vector2D &lhs, const double& rhs) {
    Vector2D vec(lhs.x * rhs, lhs.y * rhs); // Creating a new vector with components equal to the corresponding components of the input vector multiplied with the input scalar
    return vec;
}

Vector2D operator*(const double& lhs, const Vector2D &rhs) {
    Vector2D vec(rhs.x * lhs, rhs.y * lhs); // Creating a new vector with components equal to the corresponding components of the input vector multiplied with the input scalar
    return vec;
}

bool operator==(const Vector2D &lhs, const Vector2D &rhs) {
    return lhs.x == rhs.x && lhs.y == rhs.y; // If the x components of both vectors are equal and the y components of both vectors are equal return true, otherwise return false
}

bool operator!=(const Vector2D &lhs, const Vector2D &rhs){
    bool logic = not ( lhs == rhs); // Set logic to be the opposite of the input vectors being equal
    return logic;
}

double& Vector2D::operator[](const int i) {
    switch (i){
        case 0: // If the input subscript is 0 return a reference to the x component
            return x;
        case 1: // If the input subscript is 1 return a reference to the y component
            return y;
        default: // Error message for invalid input
            cerr << "Index not in bounds\n";
            break;
    }
}

