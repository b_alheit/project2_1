#include "Circle.h"
#include <cmath>

Circle::Circle() {
    centre = Point(0, 0); // Setting the center of the circle to (0,0)
    r = 0; // Setting the radius of the circle to 0
}

Circle::Circle(Point centreInit, double rInit) {
    centre = centreInit; // Setting the center of the circle to the input point
    r = rInit; // Setting the radius of the circle to the input radius
}

Circle::Circle(Point p1, Point p2, Point p3) {
    Vector2D c1(p1), c2(p2), c3(p3); // Creating three vectors with components with the values of input points

    // Applying equations to calculate the position of the center of the circle and the radius of the circle
    double Y13 = c1[1] - c3[1];
    double Y23 = c2[1] - c3[1];
    double X13 = c1[0] - c3[0];
    double X23 = c2[0] - c3[0];
    double P13 = pow(c1.norm(),2) - pow(c3.norm(),2);
    double P23 = pow(c2.norm(),2) - pow(c2.norm(),2);

    centre.set_x((Y13*P23 - Y23*P13) / (2*(X23*Y13 - X13*Y23))); // Setting the x position of the center of the circle
    centre.set_y((P23 - 2*X23*centre.get_x())/(2*Y23)); // Setting the y position of the center of the circle
    r = sqrt(pow((c1[0]-centre.get_x()),2)+pow((c1[1]-centre.get_y()),2)); // Setting the radius of the circle
}

Point Circle::get_centre() {return centre;} // Return the center of the circle

double Circle::get_r() {return r;} // Return the radius of the circle
