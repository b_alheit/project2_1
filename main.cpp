#include <iostream>
#include "Point.h"
#include "Vector2D.h"
#include "Circle.h"

int main() {
    Vector2D vec1, vec2; // Initialising two vector objects

    cout << "Enter values for vec1: " << endl;
    cin >> vec1; // Prompting user to enter values for vec1
    cout << "vec1 is: " << vec1 << endl; // Displaying vec1

    cout << "Enter values for vec2: " << endl;
    cin >> vec2; // Prompting user to enter values for vec2
    cout << "vec2 is: " << vec2 << endl; // Displaying vec2

    cout << "vec1 + vec2 : " << vec1 + vec2 << endl; // Displaying vector addition functionality
    cout << "vec1 - vec2 : " << vec1 - vec2 << endl; // Displaying vector subtraction functionality
    cout << "vec1 * vec2 : " << vec1 * vec2 << endl; // Displaying dot product functionality
    cout << "5 * vec1 : " << 5 * vec1 << endl; // Displaying scalar multiplying functionality
    cout << "vec2 * 5 : " << vec2 * 5<< endl; // Displaying scalar multiplying functionality

    if(vec1 == vec2){cout << "vec1 == vec2: " << "True" << endl;} // Displaying == boolean functionality
    else{cout << "vec1 == vec2: " << "False" << endl;}
    if(vec1 != vec2){cout << "vec1 != vec2: " << "True" << endl;} // Displaying != boolean functionality
    else{cout << "vec1 != vec2: " << "False" << endl;}

    cout << "vec1.min(): " << vec1.min() << endl; // Displaying min() method
    cout << "vec1.max(): " << vec1.max() << endl; // Displaying max() method
    cout << "vec1.norm(): " << vec1.norm() << endl; // Displaying norm() method
    cout << "vec1[0]: " << vec1[0] << endl; // Subscript operator functionality
    cout << "vec1[1]: " << vec1[1] << endl;

    Point p1(1,0), p2(0,1), p3(-1,0); // Initialising 3 points
    Circle circumscribed_circle(p1, p2, p3); // Constructing circle using three circumscribed points
    cout << "The center of the circle with circumscribed points " << p1 << ", " << p2 <<", " << p3 << " is: " << circumscribed_circle.get_centre() << endl;
    cout << "The radius of the circle with circumscribed points " << p1 << ", " << p2 <<", " << p3 << " is: " << circumscribed_circle.get_r() << endl;

    Circle known_circle(Point(1,3), 5); // Constructing circle using centre and radius
    cout << "The center of the circle constructed by \"Circle known_circle(Point(1,3), 5)\": " << known_circle.get_centre() << endl;
    cout << "The radius of the circle constructed by \"Circle known_circle(Point(1,3), 5)\": " << known_circle.get_r() << endl;
    return 0;
}