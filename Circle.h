#ifndef PROJECT2_CIRCLE_H
#define PROJECT2_CIRCLE_H

#include "Point.h"
#include "Vector2D.h"
/**
 * \class Circle
 * \brief A data type representing a circle by its center and its radius
 *
 */
class Circle{
public:
    /**
     * \brief Default constructor for circle object. Creates Circle object with initial center at (0,0) and an initial radius of 0
     */
    Circle();
    /**
     * \brief Constructor for circle object based on a given center and radius.
     *
     * @param centreInit Initial point of the center of the circle
     * @param rInit Initial radius of the circle
     */
    Circle(Point centreInit, double rInit);
    /**
     * \brief Constructor for circle object based on three circumscribed points.
     *
     * @param p1 First point on its circumference
     * @param p2 Second point on its circumference
     * @param p3 Third point on its circumference
     */
    Circle(Point p1, Point p2, Point p3);
    /**
     * \brief Method to get the center of the Circle object.
     *
     * @return The center of the circle object.
     */
    Point get_centre();
    /**
     * \brief Method to get the radius of the Circle object.
     *
     * @return The radius of the circle object.
     */
    double get_r();

private:

    Point centre; /*!< \brief The center of the circle (a point)*/
    double r; /*!< \brief The radius of the circle*/

};


#endif //PROJECT2_CIRCLE_H
